/**
 * @provides snipe-fields-validation-js
 */

/**
 * The code in this JavaScript file provides UI support for Collabora's
 * Phabricator to Snipe-IT plugin.  The PHP code for the plugin can be found
 * at:
 *
 * https://gitlab.collabora.com/phabricator/asset-item-application
 */

function triggerEvent(elem, event) {
  var clickEvent = new Event(event); // Create the event.
  elem.dispatchEvent(clickEvent); // Dispatch the event.
}

function shipping_validations() {
  let styles = `
    .snipe_assets_select {
      display: none;
    }
    .aphront-form-caption {
      clear: both;
    }
  `;

  var styleSheet = document.createElement("style");
  styleSheet.type = "text/css";
  styleSheet.innerText = styles;
  document.head.appendChild(styleSheet);

  // Hide the assets tag field, as the user shouldn't be able to edit
  // it manually.
  asset_id = document.getElementsByName("std:maniphest:shipping:asset-id")[0];
  asset_id.parentNode.parentNode.style.display = "none";

  // Set selected values on init
  const assets_element = document.getElementById("snipe_assets");

  let asset_ids_array = asset_id.value.split(",");
  for (var i = 0; i < assets_element.options.length; i++) {
    assets_element.options[i].selected =
      asset_ids_array.indexOf(assets_element.options[i].value) >= 0;
  }

  // store assets details in an object
  asset_details = {};
  for (let index = 0; index < assets_element.options.length; index++) {
    const option = assets_element.options[index];
    asset_details[option.value] = {
      href: option.attributes["data-src"]
        ? option.attributes["data-src"].value
        : "",
      country_of_origin: option.attributes["data-country-of-origin"]
        ? option.attributes["data-country-of-origin"].value
        : "",
      currency: option.attributes["data-currency"]
        ? option.attributes["data-currency"].value
        : "",
      purchase_cost: option.attributes["data-purchase-cost"]
        ? option.attributes["data-purchase-cost"].value
        : "",
    };
  }

  const choices = new Choices(assets_element, {
    removeItemButton: true,
    placeholder: true,
    placeholderValue: "Select the assets to ship",
    searchPlaceholderValue: "Select the assets to ship",
    noResultsText: "No assets found",
    searchResultLimit: 30,
    sortFields: ["label"],
    fuseOptions: {
      findAllMatches: true,
      threshold: 0.3,
      distance: 100,
      location: 30,
      useExtendedSearch: true,
    },
  });

  items_container = document.createElement("div");
  items_container.setAttribute("id", "items_container");

  // Get elements that will be modified by the select field
  let import_asset = document.getElementsByName(
    "std:maniphest:shipping:import-asset"
  )[0];
  let el_assets_value = document.getElementsByName(
    "std:maniphest:shipping:item-value"
  )[0];
  let el_country_of_origin = document.getElementsByName(
    "std:maniphest:shipping:item-origin"
  )[0];
  let el_currency = document.getElementsByName(
    "std:maniphest:shipping:currency"
  )[0];
  let el_description = document.getElementsByName(
    "std:maniphest:shipping:item-description"
  )[0];

  assets_element.addEventListener(
    "change",
    function (event) {
      document.querySelectorAll(".item_card").forEach((e) => e.remove());
      el_description.value = "";

      let values = Array.from(assets_element.selectedOptions).map(
        (v) => v.value
      );
      let items = Array.from(assets_element.selectedOptions).map((option) => {
        return {
          name: option.textContent,
          href: asset_details[option.value].href,
          props: {
            purchase_cost: {
              value: asset_details[option.value].purchase_cost,
              label: "Purchase cost",
            },
            currency: {
              value: asset_details[option.value].currency,
              label: "Currency",
            },
            country_of_origin: {
              value: asset_details[option.value].country_of_origin,
              label: "Country of origin",
            },
          },
        };
      });

      // Create item card when user select an item from the list
      items.forEach(function (item) {
        item_element = document.createElement("div");
        item_element.className = "item_card";
        item_url = document.createElement("a");
        item_url.textContent = item["name"];
        item_url.setAttribute("target", "_blank");
        item_url.setAttribute("href", item["href"]);
        item_element.appendChild(item_url);
        el_description.value = el_description.value + item["name"];
        for (var key in item.props) {
          prop = document.createElement("p");
          prop.textContent =
            item.props[key].label + ": " + item.props[key].value;
          item_element.appendChild(prop);

          el_description.value = el_description.value + "\n" + prop.textContent;
        }

        items_container.appendChild(item_element);
        el_description.value = el_description.value + "\n\n";

        if (items.length == 1) {
          el_country_of_origin.value = item.props.country_of_origin.value;
          el_currency.value = item.props.currency.value;
        }
      });

      if (items.length > 1) {
        el_country_of_origin.value = "";
        el_currency.value = "";
      }

      assets_element.parentNode.parentNode.parentNode.appendChild(
        items_container
      );

      el_assets_value.value = items.reduce(function (sum, i) {
        return sum + Number(i.props.purchase_cost.value.replaceAll(",", ""));
      }, 0);
      asset_id.value = values;
    },
    false
  );

  // if is shipped to customer, hide the Recipient user field
  let ship_to_customer = document.getElementsByName(
    "std:maniphest:shipping:ship-to-customer"
  )[0];
  ship_to_customer.onchange = function () {
    if (ship_to_customer.checked) {
      ship_to_customer.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.nextSibling.style.display =
        "none";
    } else {
      ship_to_customer.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.nextSibling.style.display =
        "block";
    }
  };

  // Hide Snipe-IT related fields if user does not intend to import details
  // from there.
  if (!import_asset.checked) {
    assets_element.parentNode.parentNode.parentNode.parentNode.style.display =
      "none";
  }
  import_asset.onchange = function () {
    if (import_asset.checked) {
      items_container.style.display = "block";
      assets_element.parentNode.parentNode.parentNode.parentNode.style.display =
        "block";
    } else {
      items_container.style.display = "none";
      assets_element.parentNode.parentNode.parentNode.parentNode.style.display =
        "none";
      items_container.innerText = "";
      assets_element.value = "";
      asset_id.value = "";
    }
  };
}

function fetch_and_hide_extra_assets_elements(index, unique_items) {
  asset = {};

  // This block of elements are used only by Snipe-IT
  asset["asset_name_parent"] = document.getElementsByName(
    "std:maniphest:purchasing:asset-name" + index
  )[0].parentNode.parentNode;
  asset["asset_name_parent"].classList.add("snipe_property");

  if (document.getElementsByName("std:maniphest:purchasing:asset-tag" + index).length) {
    asset["asset_tag_parent"] = document.getElementsByName(
      "std:maniphest:purchasing:asset-tag" + index
    )[0].parentNode.parentNode;
    asset["asset_tag_parent"].classList.add("snipe_property");
  }

  if (document.getElementsByName("std:maniphest:purchasing:item-owner" + index).length > 0) {
    asset["item_owner_parent"] = document.getElementsByName(
      "std:maniphest:purchasing:item-owner" + index
    )[0].parentNode.parentNode;
  }
  asset["item_owner_parent"].classList.add("snipe_property");

  asset["model_parent"] = document.getElementsByName(
    "std:maniphest:purchasing:model" + index
  )[0].parentNode.parentNode;
  asset["model_parent"].classList.add("snipe_property");

  // Elements used by Phab and Snipe-IT
  asset["separator"] = asset["asset_name_parent"].previousSibling;
  asset["with"] = asset["item_owner_parent"].nextSibling;
  asset["quantity"] = document.getElementsByName(
    "std:maniphest:purchasing:quantity" + index
  )[0].parentNode.parentNode;
  if (document.getElementsByName("std:maniphest:purchasing:country-origin" + index).length > 0) {
    asset["country_of_origin_parent"] = document.getElementsByName(
      "std:maniphest:purchasing:country-origin" + index
    )[0].parentNode.parentNode;
  }
  asset["asset_cost_parent"] = document.getElementsByName(
    "std:maniphest:purchasing:expected-cost" + index
  )[0].parentNode.parentNode;
  if (document.getElementsByName("std:maniphest:purchasing:item-link" + index).length > 0) {
    asset["asset_link_parent"] = document.getElementsByName(
      "std:maniphest:purchasing:item-link" + index
    )[0].parentNode.parentNode;
  };
  if (document.getElementsByName("std:maniphest:purchasing:currency" + index).length > 0) {
    asset["asset_currency_parent"] = document.getElementsByName(
      "std:maniphest:purchasing:currency" + index
    )[0].parentNode.parentNode;
  };
  if (document.getElementsByName("std:maniphest:purchasing:serial" + index).length > 0) {
    asset["asset_serial_parent"] = document.getElementsByName(
      "std:maniphest:purchasing:serial" + index
    )[0].parentNode.parentNode;
  };

  Object.keys(asset).forEach((i) => asset[i].classList.add("asset_property"));

  // Hide all extra items at first
  if (index > 1 && index > unique_items) {
    Object.keys(asset).forEach((i) => (asset[i].style.display = "none"));
  }

  return asset;
}

function purchasing_validations() {
  let styles = `
      .add-item {
        float:left !important;
      }
  `;

  var styleSheet = document.createElement("style");
  styleSheet.type = "text/css";
  styleSheet.innerText = styles;
  document.head.appendChild(styleSheet);

  num_asset_forms = document.getElementsByName(
    "std:maniphest:purchasing:unique-items"
  )[0];
  if (num_asset_forms.length) {
    // If this field was not rendered, we can't make the validations and control
    // the form based on assets amount.
    return;
  }
  num_asset_forms.parentNode.parentNode.style.display = "none";

  assets_list = [];
  MAX_ALLOWED_ASSETS = 5;

  for (let i = 1; i <= MAX_ALLOWED_ASSETS; i++) {
    assets_list.push(
      fetch_and_hide_extra_assets_elements(i, num_asset_forms.value)
    );
  }

  let should_create = document.getElementsByName(
    "std:maniphest:purchasing:create-item"
  )[0];

  assets_properties = document.getElementsByClassName("asset_property");
  snipe_it_properties = document.getElementsByClassName("snipe_property");

  if (!should_create.checked) {
    for (let i = 0; i < snipe_it_properties.length; i++) {
      snipe_it_properties[i].style.display = "none";
    }
  }

  should_create.onchange = function () {
    if (should_create.checked) {
      for (let i = 0; i < num_asset_forms.value; i++) {
        for (let key in assets_list[i]) {
          if (assets_list[i][key].classList.contains("snipe_property")) {
            assets_list[i][key].style.display = "block";
          }
        }
      }
    } else {
      for (let i = 0; i < num_asset_forms.value; i++) {
        for (let key in assets_list[i]) {
          if (assets_list[i][key].classList.contains("snipe_property")) {
            assets_list[i][key].style.display = "none";
          }
        }
      }
    }
  };

  if (document.getElementsByName("__submit__").length === 0) {
    return;
  }
  submit_parent = document.getElementsByName("__submit__")[0].parentNode
    .parentNode;
  add_new_asset = document.createElement("div");
  add_new_asset_btn = document.createElement("a");
  add_new_asset_btn.classList.add("button", "add-item");
  add_new_asset_btn.innerHTML = "Add another item to this ticket";
  add_new_asset.appendChild(add_new_asset_btn);

  add_new_asset_btn.onclick = function (e) {
    e.preventDefault();
    if (!num_asset_forms.value) {
      num_asset_forms.value = 1;
    };
    if (num_asset_forms.value < MAX_ALLOWED_ASSETS) {
      for (let key in assets_list[num_asset_forms.value - 1]) {
        if (
          assets_list[num_asset_forms.value][key].classList.contains(
            "snipe_property"
          )
        ) {
          if (should_create.checked) {
            assets_list[num_asset_forms.value][key].style.display = "block";
          }
        } else {
          assets_list[num_asset_forms.value][key].style.display = "block";
        }
      }

      num_asset_forms.value++;

      // Auto fill currency and ship to fields based on the first item
      document.getElementsByName(
        "std:maniphest:purchasing:currency" + num_asset_forms.value
      )[0].value = document.getElementsByName(
        "std:maniphest:purchasing:currency1"
      )[0].value;
    } else {
      add_new_asset_btn.innerText = "Items per ticket exceeded";
      add_new_asset_btn.classList.add("disabled");
    }
  };

  submit_parent.appendChild(add_new_asset);
}

document.addEventListener("DOMContentLoaded", function (event) {
  /* Make sure this will load only in the Purchase and Shipping forms */
  let purchasing_element = document.getElementsByName(
    "std:maniphest:purchasing:asset-name1"
  );
  let shipping_element = document.getElementsByName(
    "std:maniphest:shipping:shipper-name"
  );

  if (purchasing_element.length) {
    try {
      purchasing_validations();
    } catch (error) {
      console.error(error);
    }
  } else if (shipping_element.length) {
    try {
      shipping_validations();
    } catch (error) {
      console.error(error);
    }
  }
});
