<?php

final class ProjectBoardTaskCard extends Phobject {

  private $viewer;
  private $projectHandles;
  private $task;
  private $owner;
  private $showEditControls;
  private $canEdit;
  private $coverImageFile;
  private $hideArchivedProjects;

  public function setViewer(PhabricatorUser $viewer) {
    $this->viewer = $viewer;
    return $this;
  }
  public function getViewer() {
    return $this->viewer;
  }

  public function setProjectHandles(array $handles) {
    $this->projectHandles = $handles;
    return $this;
  }

  public function getProjectHandles() {
    return $this->projectHandles;
  }

  public function setCoverImageFile(PhabricatorFile $cover_image_file) {
    $this->coverImageFile = $cover_image_file;
    return $this;
  }

  public function getCoverImageFile() {
    return $this->coverImageFile;
  }

  public function setHideArchivedProjects($hide_archived_projects) {
    $this->hideArchivedProjects = $hide_archived_projects;
    return $this;
  }

  public function getHideArchivedProjects() {
    return $this->hideArchivedProjects;
  }

  public function setTask(ManiphestTask $task) {
    $this->task = $task;
    return $this;
  }
  public function getTask() {
    return $this->task;
  }

  public function setOwner(PhabricatorObjectHandle $owner = null) {
    $this->owner = $owner;
    return $this;
  }
  public function getOwner() {
    return $this->owner;
  }

  public function setCanEdit($can_edit) {
    $this->canEdit = $can_edit;
    return $this;
  }

  public function getCanEdit() {
    return $this->canEdit;
  }

  public function setShowEditControls($show_edit_controls) {
    $this->showEditControls = $show_edit_controls;
    return $this;
  }

  public function getShowEditControls() {
    return $this->showEditControls;
  }

  public function getItem() {
    $task = $this->getTask();
    $owner = $this->getOwner();
    $can_edit = $this->getCanEdit();
    $viewer = $this->getViewer();

    $color_map = ManiphestTaskPriority::getColorMap();
    $bar_color = idx($color_map, $task->getPriority(), 'grey');

    $card = id(new PHUIObjectItemView())
      ->setObject($task)
      ->setUser($viewer)
      ->setObjectName($task->getMonogram())
      ->setHeader($task->getTitle())
      ->setHref($task->getURI())
      ->addSigil('project-card')
      ->setDisabled($task->isClosed())
      ->setBarColor($bar_color);

    if ($this->getShowEditControls()) {
      if ($can_edit) {
        $card
          ->addSigil('draggable-card')
          ->addClass('draggable-card');
        $edit_icon = 'fa-pencil';
      } else {
        $card
          ->addClass('not-editable')
          ->addClass('undraggable-card');
        $edit_icon = 'fa-lock red';
      }

      $card->addAction(
        id(new PHUIListItemView())
          ->setName(pht('Edit'))
          ->setIcon($edit_icon)
          ->addSigil('edit-project-card')
          ->setHref('/maniphest/task/edit/'.$task->getID().'/'));
    }

    if ($owner) {
      $card->addHandleIcon($owner, $owner->getName());
    }

    $cover_file = $this->getCoverImageFile();
    if ($cover_file) {
      $card->setCoverImage($cover_file->getBestURI());
    }

    if (ManiphestTaskPoints::getIsEnabled()) {
      $points = $task->getPoints();
      if ($points !== null) {
        $points_tag = id(new PHUITagView())
          ->setType(PHUITagView::TYPE_SHADE)
          ->setColor(PHUITagView::COLOR_GREY)
          ->setSlimShady(true)
          ->setName($points)
          ->addClass('phui-workcard-points');
        $card->addAttribute($points_tag);
      }
    }

    $subtype = $task->newSubtypeObject();
    if ($subtype && $subtype->hasTagView()) {
      $subtype_tag = $subtype->newTagView()
        ->setSlimShady(true);
      $card->addAttribute($subtype_tag);
    }

    $status = $task->getStatus();
    $status_icon = ManiphestTaskStatus::getStatusIcon($status);
    if (ManiphestTaskStatus::isClosedStatus($status)) {
      $status_color = 'grey';
    } else {
      $status_color = 'dark';
    }
    $card->setStatusIcon($status_icon.' '.$status_color);

    $review_status = $task->getReviewIcon($viewer);
    if ($review_status) {
      $review_tag = id(new PHUITagView())
        ->setType(PHUITagView::TYPE_OBJECT)
        ->setBackgroundColor(null)
        ->setIcon($review_status);
      $card->addAttribute($review_tag);
    }

    $project_handles = $this->getProjectHandles();

    // Remove any archived or hidden projects from the list.
    if ($project_handles) {
      foreach ($project_handles as $key => $handle) {
        if ($handle->getPolicyFiltered() ||
	    ($this->hideArchivedProjects &&
	     $handle->getStatus() == PhabricatorObjectHandle::STATUS_CLOSED)) {
          unset($project_handles[$key]);
        }
      }
    }

    if ($project_handles) {
      $project_handles = array_reverse($project_handles);
      $tag_list = id(new PHUIHandleTagListView())
        ->setSlim(true)
        ->setHandles($project_handles);
      $card->addAttribute($tag_list);
    }

    $card->addClass('phui-workcard');

    // CCU: display the RAG indicator and progress bar in the task card
    $rag_status = $task->getRAGStatus();
    if ($rag_status) {
      $rag_icon = id(new PHUITagView())
        ->setType(PHUITagView::TYPE_OBJECT)
        ->setBackgroundColor(null)
        ->setIcon('fa-circle ' . $rag_status);
      $card->addAttribute($rag_icon);

      $task_children = $task->loadDependsOnTaskPHIDs();
      $task_progress = [];
      if ($task_children) {
        $task_progress = $task->getTaskProgress($viewer);
      }
      if ($task_children && $task_progress['total_points'] > 0) {
        $bar = id(new PHUISegmentBarView())
          ->setLabel(pht(
            'Progress: %s/%s points',
            new PhutilNumber($task_progress['completed_points']),
            new PhutilNumber($task_progress['total_points'])
          ));

        $bar->newSegment()
          ->setWidth($task_progress['completed_points'] / $task_progress['total_points'])
          ->setColor('green')
          ->setTooltip(pht(
            '%s/%s complete',
            new PhutilNumber($task_progress['completed_points']),
            new PhutilNumber($task_progress['total_points']))
          );

          $bar->newSegment()
          ->setWidth($task_progress['in_progress'] / $task_progress['total_points'])
          ->setColor('blue')
          ->setTooltip(pht(
            '%s/%s in progress',
            new PhutilNumber($task_progress['in_progress']),
            new PhutilNumber($task_progress['total_points']))
          );

        $bar = phutil_tag(
          'div',
          array(
            'class' => 'phui-profile-segment-bar',
          ),
          $bar);
        $card->addAttribute($bar);
      }
    }


    return $card;
  }

}
